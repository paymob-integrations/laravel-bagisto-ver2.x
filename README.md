# Paymob Payment Gateway package for Bagisto Version 2.x Based on Laravel

## Installation
1. Install the Paymob Payment module for Laravel Bagisto 2.x e-commerce via [paymob/laravel-bagisto2.x](https://packagist.org/packages/paymob/laravel-bagisto2.x) composer.
```bash
composer require paymob/laravel-bagisto2.x
```

2. In the command line, run the below commands
```bash
php artisan vendor:publish --force --tag=paymob 
php artisan migrate 
php artisan optimize 
```

3. Goto app/Http/Middleware/VerifyCsrfToken.php file. Then, add **paymob/callback** in the protected array $except as below
```php
    protected $except = ['paymob/callback',];
```

4. In the command line, run the below command
```bash
php artisan config:cache
```

## Configuration
### Paymob Account
1. Login to the Paymob account → Setting in the left menu. 
2. Get the Secret, public, API keys, HMAC and Payment Methods IDs (integration IDs).

### Bagisto Admin Configuration
1. In Bagisto Admin Panel Menu configuration→ sales→ paymentmethods. 
2. Search for Paymob payment, paste each key in its place in the setting page. 
3. Please ensure adding the integration IDs separated by comma ,. These IDs will be shown in the Paymob payment page. 
4. Copy integration callback URL that exists in Paymob Bagisto setting page. Then, paste it into each payment integration/method in Paymob account.

## Checkout page 
Paymob payment method will be shown for the end-user to select and start his payment process. 
