<?php

use Illuminate\Support\Facades\Route;
use Bagisto\Paymob\Controllers\PaymobController;

Route::group([
    //   'prefix'     => 'paymob',
       'middleware' => ['web', 'theme', 'locale', 'currency']
   ], function () {
       Route::get('redirect',[PaymobController::class, 'redirect'])->name('paymob.process');
       Route::any('paymob/callback',[PaymobController::class, 'callback'])->name('paymob.callback'); 
});