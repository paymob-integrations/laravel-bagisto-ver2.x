<?php

return [
    [
        'key' => 'sales.payment_methods.paymob',
        'name' => 'paymob::app.system.title',
        'info' => 'paymob::app.system.info',
        'sort' => 3,
        'fields' => [
            [
                'name' => 'title',
                'title' => 'paymob::app.system.p-title',
                'type' => 'depends',
                'depend' => 'active:1',
                'validation' => 'required_if:active,1',
                'channel_based' => false,
                'locale_based' => true,
            ],
            [
                'name' => 'description',
                'title' => 'paymob::app.system.p-description',
                'type' => 'textarea',
                'channel_based' => false,
                'locale_based' => true,
            ],
            [
                'name' => 'public-key',
                'title' => 'paymob::app.system.public-key',
                'type' => 'depends',
                'depend' => 'active:1',
                'validation' => 'required_if:active,1',
                'channel_based' => false,
                'locale_based' => true,
            ],
            [
                'name' => 'secret-key',
                'title' => 'paymob::app.system.secret-key',
                'type' => 'depends',
                'depend' => 'active:1',
                'validation' => 'required_if:active,1',
                'channel_based' => false,
                'locale_based' => true,
            ],
            [
                'name' => 'integration-ids',
                'title' => 'paymob::app.system.integration-ids',
                'type' => 'depends',
                'depend' => 'active:1',
                'validation' => 'required_if:active,1',
                'channel_based' => false,
                'locale_based' => true,
                'info' => 'Add the Payment methods ID(s) that exist in Paymob account separated by comma , separator. (Example: 123456,98765,45678)'
            ],
            [
                'name' => 'hmac',
                'title' => 'paymob::app.system.hmac',
                'type' => 'depends',
                'depend' => 'active:1',
                'validation' => 'required_if:active,1',
                'channel_based' => false,
                'locale_based' => true,
            ],
            [
                'name' => 'paymobcallback',
                'title' => 'paymob::app.system.callback-url',
                'type' => 'hidden',
                'channel_based' => false,
                'locale_based' => true,
                'info' => ' Transaction Processed/Response Callback URL: ' . config('app.url') . '/paymob/callback ',
            ],
            [
                'name' => 'debug-log',
                'title' => 'paymob::app.system.debug-log',
                'type' => 'boolean',
                'channel_based' => false,
                'locale_based' => true,
                'info' => 'Log file will be saved in ' . storage_path() . '/logs/'
            ],
            [
                'name' => 'active',
                'title' => 'admin::app.configuration.index.sales.payment-methods.status',
                'type' => 'boolean',
                'channel_based' => true,
                'locale_based' => false,
            ],
            [
                'name' => 'sort',
                'title' => 'paymob::app.system.sort-order',
                'type' => 'select',
                'options' => [
                    [
                        'title' => '1',
                        'value' => 1,
                    ],
                    [
                        'title' => '2',
                        'value' => 2,
                    ],
                    [
                        'title' => '3',
                        'value' => 3,
                    ],
                    [
                        'title' => '4',
                        'value' => 4,
                    ],
                    [
                        'title' => '5',
                        'value' => 5,
                    ],
                ],
            ],
        ]
    ]
];