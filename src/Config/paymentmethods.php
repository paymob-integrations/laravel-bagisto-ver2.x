<?php

return [
    'paymob'  => [
        'code'        => 'paymob',
        'title'       => 'Paymob',
        'description' => 'Paymob Payment Method for Bagisto',
        'class'       => 'Bagisto\Paymob\Payment\Paymob',
        'active'      => false,
        'sort'        => 4
    ],
];