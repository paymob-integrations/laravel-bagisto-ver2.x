<?php

return [
    'system' => [
        'title' => 'Paymob',
        'info' => 'Paymob is a payment service provider that provides a user-friendly hosted checkout where customers can select any payment method defined by the merchant.',
        'p-title' => 'Title',
        'p-description' => 'Description',
        'public-key' => 'Public Key',
        'secret-key' => 'Secret Key',
        'integration-ids' => 'Payment method(s)',
        'hmac' => 'HMAC',
        'paymobstatus' => 'Status',
        'callback-url' => 'Callback URL',
        'debug-log' => 'Debug Log',
        'sort-order' => 'Sort Order'
    ]
];