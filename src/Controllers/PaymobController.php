<?php

namespace Bagisto\Paymob\Controllers;

use Webkul\Checkout\Facades\Cart;
use Webkul\Sales\Repositories\OrderRepository;
use Webkul\Sales\Repositories\InvoiceRepository;
use Webkul\Payment\Payment\Payment;
use Illuminate\Routing\Controller;
use Paymob\Library\Paymob;

class PaymobController extends Controller
{
    protected $publickey;
    protected $secretkey;
    protected $integrationIds;
    protected $hmac;
    protected $file;
    protected $debug;
    /**
     * OrderRepository $orderRepository
     *
     * @var \Webkul\Sales\Repositories\OrderRepository
     */
    protected $orderRepository;

    /**
     * InvoiceRepository $invoiceRepository
     *
     * @var \Webkul\Sales\Repositories\InvoiceRepository
     */
    protected $invoiceRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Attribute\Repositories\OrderRepository  $orderRepository
     * @return void
     */
    public function __construct(OrderRepository $orderRepository, InvoiceRepository $invoiceRepository)
    {
        $this->orderRepository = $orderRepository;
        $this->invoiceRepository = $invoiceRepository;
        $this->publickey = core()->getConfigData('sales.payment_methods.paymob.public-key');
        $this->secretkey = core()->getConfigData('sales.payment_methods.paymob.secret-key');
        $this->integrationIds = core()->getConfigData('sales.payment_methods.paymob.integration-ids');
        $this->hmac = core()->getConfigData('sales.payment_methods.paymob.hmac');
        $this->file = storage_path() . "/logs/paymob.log";
        $this->debug = core()->getConfigData('sales.payment_methods.paymob.debug-log') == 'on' ? '1' : '0';
    }

    /**
     * Redirects to the Paymob Hosted Checkout.
     *
     * @return \Illuminate\View\View
     */

    public function redirect()
    {
        $cart = Cart::getCart();
        $order = $this->orderRepository->create(Cart::prepareDataForOrder());
        $order = $this->orderRepository->find($order->id);
        $orderId = $order->id;
        $currency = $order->order_currency_code;
        $integrations = explode(',', $this->integrationIds);
        $integration_ids = [];
        foreach ($integrations as $id) {
            $id = (int) $id;
            if ($id > 0) {
                array_push($integration_ids, $id);
            }
        }
        $billingAddress = $cart->billing_address;
        $billing = [
            "email" => $billingAddress->email,
            "first_name" => $billingAddress->first_name,
            "last_name" => $billingAddress->last_name,
            "street" => !empty($billingAddress->address1) ? $billingAddress->address1 . ' - ' . $billingAddress->address2 : 'NA',
            "phone_number" => !empty($billingAddress->phone) ? $billingAddress->phone : 'NA',
            "city" => !empty($billingAddress->city) ? $billingAddress->city : 'NA',
            "country" => !empty($billingAddress->country) ? $billingAddress->country : 'NA',
            "state" => !empty($billingAddress->state) ? $billingAddress->state : 'NA',
            "postal_code" => !empty($billingAddress->postcode) ? $billingAddress->postcode : 'NA',
        ];
        $price = $order->grand_total;
        $country = Paymob::getCountryCode($this->secretkey);
        $cents = 100;
        $round = 2;
        if ($country == 'omn') {
            $cents = 1000;
        }
        $price = round((round($price, $round)) * $cents, $round);
        $data = [
            "amount" => $price,
            "currency" => $currency,
            "payment_methods" => $integration_ids,
            "billing_data" => $billing,
            "extras" => ["merchant_intention_id" => $orderId . '_' . time()],
            "special_reference" => $orderId . '_' . time()
        ];
        $paymobReq = new Paymob($this->debug, $this->file);
        $status = $paymobReq->createIntention($this->secretkey, $data, $orderId);
        if (!$status['success']) {
            session()->flash('error', $status['message']);
            return redirect()->route('shop.checkout.cart.index');
        } else {
            $countryCode = $paymobReq->getCountryCode($this->secretkey);
            $apiUrl = $paymobReq->getApiUrl($countryCode);
            $cs = $status['cs'];
            $url = $apiUrl . "unifiedcheckout/?publicKey=$this->publickey&clientSecret=$cs";
            $order->PaymobIntentionId = $status['intentionId'];
            $order->PaymobCentsAmount = $status['centsAmount'];
            $order->save();
            return redirect($url);
        }
    }

    /***
     * 
     * Call back url
     */
    public function callback()
    {
        if (Paymob::filterVar('REQUEST_METHOD', 'SERVER') === 'POST') {
            $post_data = file_get_contents('php://input');
            $json_data = json_decode($post_data, true);
            $orderId = Paymob::getIntentionId($json_data['intention']['extras']['creation_extras']['merchant_intention_id']);
            Paymob::addLogs($this->debug, $this->file, ' In Webhook action, for order# ' . $orderId, json_encode($json_data));
            if (empty($orderId) || is_null($orderId) || $orderId === false || $orderId === "") {
                die("Ops, you are accessing wrong data");
            }
            $order = $this->orderRepository->find($orderId);
            $OrderIntensionId = !empty($order->PaymobIntentionId) ? $order->PaymobIntentionId : null;
            $OrderAmount = !empty($order->PaymobCentsAmount) ? $order->PaymobCentsAmount : null;
            if ($OrderIntensionId != $json_data['intention']['id']) {
                die("intension ID is not matched for order : $orderId");
            }
            if ($OrderAmount != $json_data['intention']['intention_detail']['amount']) {
                die("intension amount are not matched for order : $orderId");
            }
            $country = Paymob::getCountryCode($this->secretkey);
            $cents = 100;
            if ($country == 'omn') {
                $cents = 1000;
            }
            if (!Paymob::verifyHmac($this->hmac, $json_data, array('id' => $OrderIntensionId, 'amount' => $OrderAmount, 'cents' => $cents))) {
                die("can not verify order: $orderId");
            }
            $msg = 'Paymob  Webhook for Order #' . $orderId;
            if (!empty($json_data['transaction'])) {
                $trans = $json_data['transaction'];
                if (
                    $trans['success'] === true &&
                    $trans['is_voided'] === false &&
                    $trans['is_refunded'] === false &&
                    $trans['is_capture'] === false
                ) {
                    $note = 'Paymob  Webhook: Payment Approved';
                    $msg = $msg . ' ' . $note;
                    Paymob::addLogs($this->debug, $this->file, $msg);
                    $this->orderRepository->update(['status' => 'processing'], $order->id);
                    if ($order->canInvoice()) {
                        $this->invoiceRepository->create($this->prepareInvoiceData($order));
                    }
                } else {
                    $note = 'Paymob Webhook: Payment is not completed ';
                    $msg = $msg . ' ' . $note;
                    Paymob::addLogs($this->debug, $this->file, $msg);
                    $this->orderRepository->update(['status' => 'canceled'], $order->id);
                }
                die("Order updated: $orderId");
            }
        } else if (Paymob::filterVar('REQUEST_METHOD', 'SERVER') === 'GET') {

            if (!Paymob::verifyHmac($this->hmac, $_GET)) {
                session()->flash('error', 'Ops, you are accessing wrong data');
                return redirect()->route('shop.checkout.cart.index');
            }
            $orderId = Paymob::getIntentionId(Paymob::filterVar('merchant_order_id'));
            Paymob::addLogs($this->debug, $this->file, ' In Callback action, for order# ' . $orderId, json_encode($_GET));
            if (empty($orderId) || is_null($orderId) || $orderId === false || $orderId === "") {
                session()->flash('error', 'Ops, you are accessing wrong data');
                return redirect()->route('shop.checkout.cart.index');
            }
            $order = $this->orderRepository->find($orderId);
            if (
                Paymob::filterVar('success') === "true" &&
                Paymob::filterVar('is_voided') === "false" &&
                Paymob::filterVar('is_refunded') === "false"
            ) {
                $note = 'Paymob : Payment Approved';
                $msg = 'In callback action, for order #' . $orderId . ' ' . $note;
                Paymob::addLogs($this->debug, $this->file, $msg);
                $this->orderRepository->update(['status' => 'processing'], $order->id);
                if ($order->canInvoice()) {
                    $this->invoiceRepository->create($this->prepareInvoiceData($order));
                }
                Cart::deActivateCart();
                return view('shop::checkout.success', compact('order'));
            } else {
                $note = 'Paymob : Payment is not completed';
                $this->orderRepository->update(['status' => 'canceled'], $order->id);
                $msg = 'In callback action, for order #' . $orderId . ' ' . $note;
                Paymob::addLogs($this->debug, $this->file, $msg);
                session()->flash('error', $note);
                return redirect()->route('shop.checkout.cart.index');
            }
        }
    }

    /**
     * Prepares order's invoice data for creation.
     *
     * @return array
     */
    protected function prepareInvoiceData($order)
    {
        $invoiceData = ["order_id" => $order->id,];
        foreach ($order->items as $item) {
            $invoiceData['invoice']['items'][$item->id] = $item->qty_to_invoice;
        }
        return $invoiceData;
    }
}